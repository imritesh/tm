# Global config

DEBUG = False
SECRET_KEY = "foobar"
ENABLE_WEB_UI = True
ENABLE_DATA_ALTERING_API = False


# Database config

DB_NAME = "amagama"
DB_USER = "postgres"
DB_PASSWORD = "root"
DB_HOST = "172.19.1.6"
DB_PORT = "5432"


# Database pool config

DB_MIN_CONNECTIONS = 2
DB_MAX_CONNECTIONS = 20


# Levenshtein config

MAX_LENGTH = 2000
MIN_SIMILARITY = 70
MAX_CANDIDATES = 5